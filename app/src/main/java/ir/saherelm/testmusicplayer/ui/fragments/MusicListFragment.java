package ir.saherelm.testmusicplayer.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ir.saherelm.testmusicplayer.HomeActivity;
import ir.saherelm.testmusicplayer.R;
import ir.saherelm.testmusicplayer.requirements.XBaseFragment;
import ir.saherelm.testmusicplayer.requirements.adapters.MusicItemAdapter;
import ir.saherelm.testmusicplayer.requirements.interfaces.XItemModelClickListener;
import ir.saherelm.testmusicplayer.requirements.models.MusicItemModel;
import ir.saherelm.testmusicplayer.requirements.providers.TempDataProvider;
import ir.saherelm.testmusicplayer.requirements.services.PlayerService;
import ir.saherelm.testmusicplayer.requirements.utils.AutoGridLayoutManager;
import ir.saherelm.testmusicplayer.ui.activities.PlayerActivity;

/**
 * Show a List of Music Items ...
 *
 * Created by SaherElm on 30/05/2018.
 */

public class MusicListFragment extends XBaseFragment {

    private final String TAG = "sh_" + getClass().getSimpleName();
    private MusicItemAdapter mAdapter;

    @SuppressWarnings("UnnecessaryLocalVariable")
    public static MusicListFragment newInstance() {
        //
        MusicListFragment mFrag = new MusicListFragment();

        //
        // Setting Arguments if Need ...

        //
        return mFrag;
    }

    //
    // region Overrided Method(s) ...
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_music_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //
        prepareUI(view, savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.i(TAG, "onStart");

        //
        fetchData();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
    // endregion

    //
    // region Private Method(s) ...
    @SuppressWarnings("unused")
    private void prepareUI(View view, Bundle savedInstanceState) {
        Log.i(TAG, "prepareUI");

        //
        RecyclerView mRvItems = view.findViewById(R.id.rv_music_items);

        //
        mRvItems.setLayoutManager(new AutoGridLayoutManager(getContext(), 3));

        //
        mAdapter = new MusicItemAdapter(new XItemModelClickListener<MusicItemModel>() {
            @Override
            public void onItemClicked(MusicItemModel itemModel) {
                Log.e(TAG, "item Clicked : " + itemModel.getTitle());

                //
                // Prevent from Starting Current Item Again ...
                if (!getHomeActivity().isPlayerBound()
                        || !getHomeActivity().getPlayerService().isPlayingMedia()
                        || !getHomeActivity().getPlayerService().getItemModel().equals(itemModel)) {
                    //
                    PlayerService.startPlaying(getContext(), itemModel);
                }

                //
                Intent intent = new Intent(getContext(), PlayerActivity.class);
                startActivity(intent);

                //
                getActivity().finish();
            }

            @Override
            public void onItemLongClicked(MusicItemModel itemModel) {

            }
        });

        //
        mRvItems.setAdapter(mAdapter);
        mRvItems.setItemAnimator(new DefaultItemAnimator());
    }

    private void fetchData() {
        Log.i(TAG, "fetchData");

        //
        mAdapter.addList(TempDataProvider.getMusicItems());
    }

    private HomeActivity getHomeActivity() {
        //
        return ((HomeActivity) getActivity());
    }
    // endregion

}
