package ir.saherelm.testmusicplayer.ui.activities;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import ir.saherelm.testmusicplayer.HomeActivity;
import ir.saherelm.testmusicplayer.R;
import ir.saherelm.testmusicplayer.requirements.XBaseActivity;
import ir.saherelm.testmusicplayer.requirements.models.MusicItemModel;
import ir.saherelm.testmusicplayer.requirements.services.PlayerService;
import ir.saherelm.testmusicplayer.requirements.views.CircularSeekBar;

import static ir.saherelm.testmusicplayer.requirements.services.PlayerService.PARAM_CURRENT_TIME;
import static ir.saherelm.testmusicplayer.requirements.services.PlayerService.PARAM_IS_PLAYING;
import static ir.saherelm.testmusicplayer.requirements.services.PlayerService.PARAM_MAX_TIME;

public class PlayerActivity extends XBaseActivity implements CircularSeekBar.OnCircularSeekBarChangeListener, View.OnClickListener {

    private final String TAG = "sh_" + getClass().getSimpleName();
    public static final String PARAM_MUSIC_ITEM = "param:music_item";

    private PlayerService mPlayerService;
    private Intent mPlayerIntent;
    private boolean isPlayerBound = false;

    private ImageView mIvMusicImage;
    private TextView mTvTitle;
    private TextView mTvSigner;
    private TextView mTvDescription;
    private CircularSeekBar mCsbMusicLength;
    private FloatingActionButton mFabPlayPause;


    //
    // region Overrided Method(s) ...
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate");

        setContentView(R.layout.activity_player);

        //
        prepareUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(TAG, "onStart");

        //
        if (mPlayerIntent == null) {
            //
            mPlayerIntent = new Intent(this, PlayerService.class);
            bindService(mPlayerIntent, mPlayerServiceConnection, Context.BIND_AUTO_CREATE);
            startService(mPlayerIntent);

            //
            registerReceiver(broadcastReceiver, new IntentFilter(PlayerService.ACTION_NOTIFY_UI));
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy");

        //
        stopService(mPlayerIntent);
        unregisterReceiver(broadcastReceiver);
        mPlayerService = null;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);

        //
        finish();
    }

    @Override
    public void onClick(View view) {
        //
        switch (view.getId()) {
            case R.id.fab_play_pause:
                handleActionPlayPause();
                break;

            case R.id.fab_stop:
                handleActionStop();
                break;
        }
    }
    // endregion

    //
    // region Circular Seekbar Change Listener ...
    @Override
    public void onProgressChanged(CircularSeekBar circularSeekBar, int progress, boolean fromUser) {
        if (fromUser) {
            int mCurrentPos = circularSeekBar.getProgress();
            mPlayerService.updateSeekPos(mCurrentPos);
        }
    }

    @Override
    public void onStopTrackingTouch(CircularSeekBar seekBar) {

    }

    @Override
    public void onStartTrackingTouch(CircularSeekBar seekBar) {

    }
    // endregion

    //
    // region private Method(s) ...
    private void prepareUI() {
        Log.i(TAG, "prepareUI");

        //
        mTvTitle = findViewById(R.id.tv_title);
        mTvSigner = findViewById(R.id.tv_signer);
        mTvDescription = findViewById(R.id.tv_description);
        mIvMusicImage = findViewById(R.id.iv_music_image);

        //
        mCsbMusicLength = findViewById(R.id.csb_music_length);

        //
        mFabPlayPause = findViewById(R.id.fab_play_pause);
        mFabPlayPause.setOnClickListener(this);

        //
        FloatingActionButton mFabStop = findViewById(R.id.fab_stop);
        mFabStop.setOnClickListener(this);

        //
        mCsbMusicLength.setOnSeekBarChangeListener(this);
    }

    private void updateUI(Intent serviceIntent) {
        Log.i(TAG, "updateUI");

        //
        int currentTime = serviceIntent.getIntExtra(PARAM_CURRENT_TIME, 0);
        int maxTime = serviceIntent.getIntExtra(PARAM_MAX_TIME, 0);
        boolean isPlaying = serviceIntent.getBooleanExtra(PARAM_IS_PLAYING, false);

        mCsbMusicLength.setMax(maxTime);
        mCsbMusicLength.setProgress(currentTime);

        if (!isPlaying) {
            Log.e(TAG, "Music Ended ...");
            mFabPlayPause.setImageResource(R.drawable.ic_play);
        } else {
            mFabPlayPause.setImageResource(R.drawable.ic_pause);
        }
    }

    private void handleActionPlayPause() {
        Log.i(TAG, "handleActionPlayPause");

        //
        if (!isPlayerBound) {
            return;
        }

        //
        if (mPlayerService.isPlayingMedia()) {
            //
            mPlayerService.pauseMedia();
            mFabPlayPause.setImageResource(R.drawable.ic_play);
        } else {
            //
            mPlayerService.playMedia();
            mFabPlayPause.setImageResource(R.drawable.ic_pause);
        }
    }

    private void handleActionStop() {
        Log.i(TAG, "handleActionStop");

        //
        if (!isPlayerBound) {
            return;
        }

        //
        mPlayerService.stopMedia();

        //
        onBackPressed();
    }
    // endregion

    //
    // region inner Definition(s) ...
    private ServiceConnection mPlayerServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            //
            PlayerService.PlayerBinder mBinder = (PlayerService.PlayerBinder) iBinder;
            mPlayerService = mBinder.getService();
            isPlayerBound = true;

            //
            MusicItemModel mMusicItemModel = mPlayerService.getItemModel();
            if (mMusicItemModel == null) {
                finish();
            }

            //
            //noinspection ConstantConditions
            mTvTitle.setText(mMusicItemModel.getTitle());
            mTvSigner.setText(mMusicItemModel.getSigner());
            mTvDescription.setText(mMusicItemModel.getDescription());

            //
            mIvMusicImage.setImageResource(mMusicItemModel.getImage());

            //
            Log.e(TAG, "Player Service Current Item : " + mMusicItemModel.getTitle());
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            //
            isPlayerBound = false;

            //
            mTvTitle.setText(null);
            mTvSigner.setText(null);
            mTvDescription.setText(null);
        }
    };

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent serviceIntent) {
            updateUI(serviceIntent);
        }
    };
    // endregion

}
