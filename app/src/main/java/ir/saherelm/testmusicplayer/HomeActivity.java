package ir.saherelm.testmusicplayer;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.util.Log;

import ir.saherelm.testmusicplayer.requirements.XBaseActivity;
import ir.saherelm.testmusicplayer.requirements.adapters.XViewPagerAdapter;
import ir.saherelm.testmusicplayer.requirements.models.MusicItemModel;
import ir.saherelm.testmusicplayer.requirements.models.XViewPagerAdapterData;
import ir.saherelm.testmusicplayer.requirements.services.PlayerService;
import ir.saherelm.testmusicplayer.requirements.utils.XViewPagerDataObserver;
import ir.saherelm.testmusicplayer.ui.fragments.MusicListFragment;

@SuppressWarnings("unused")
public class HomeActivity extends XBaseActivity {

    private final String TAG = "sh_" + getClass().getSimpleName();

    private ViewPager mViewPager;
    private TabLayout mTabLayout;

    private PlayerService mPlayerService;
    private Intent mPlayerIntent;
    private boolean isPlayerBound = false;

    private MusicItemModel mMusicItemModel;


    //
    // region Overrided Method(s) ...
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //
        setContentView(R.layout.activity_home);
        initToolbar(R.id.activity_toolbar);

        //
        prepareUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(TAG, "onStart");

        //
        if (mPlayerIntent == null) {
            //
            mPlayerIntent = new Intent(this, PlayerService.class);
            bindService(mPlayerIntent, mPlayerServiceConnection, Context.BIND_AUTO_CREATE);
            startService(mPlayerIntent);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy");

        //
        stopService(mPlayerIntent);
        mPlayerService = null;
    }
    // endregion

    private void prepareUI() {
        Log.i(TAG, "prepareUI");

        //
        XViewPagerAdapter mAdapter = new XViewPagerAdapter(getSupportFragmentManager(),
                new XViewPagerAdapterData[]{
                        new XViewPagerAdapterData(MusicListFragment.newInstance(), getString(R.string.fragment_music_item_list_title), R.drawable.ic_star),
                        new XViewPagerAdapterData(MusicListFragment.newInstance(), getString(R.string.fragment_music_item_list_title), R.drawable.ic_star),
                        new XViewPagerAdapterData(MusicListFragment.newInstance(), getString(R.string.fragment_music_item_list_title), R.drawable.ic_star)
                });

        //
        mViewPager = findViewById(R.id.vp_sections);
        mViewPager.setOffscreenPageLimit(3);

        //
        mTabLayout = findViewById(R.id.tab_layout_sections);
        mTabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        mTabLayout.setTabMode(TabLayout.MODE_FIXED);
        mTabLayout.setupWithViewPager(mViewPager, true);

        //
        ViewPager.OnPageChangeListener mPageChangeListener = new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                for (int i = 0; i < mTabLayout.getTabCount(); i++) {
                    TabLayout.Tab mTab = mTabLayout.getTabAt(i);
                    if (mTab != null
                            && mTab.getCustomView() != null) {
                        mTab.getCustomView().setAlpha(i == position ? 1F : 0.5F);
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        };

        //
        mViewPager.addOnPageChangeListener(mPageChangeListener);
        XViewPagerDataObserver mViewPagerObserver = new XViewPagerDataObserver(mTabLayout, mAdapter);
        mAdapter.registerDataSetObserver(mViewPagerObserver);

        //
        mViewPager.setAdapter(mAdapter);
        mViewPagerObserver.updateTabViews();
        mViewPager.post(new Runnable() {
            @Override
            public void run() {
                mViewPager.setCurrentItem(1, false);
                mViewPager.setCurrentItem(0, true);
            }
        });
    }

    //
    // region Getter(s) ...
    public PlayerService getPlayerService() {
        return mPlayerService;
    }

    public boolean isPlayerBound() {
        return isPlayerBound;
    }

    public MusicItemModel getmMusicItemModel() {
        return mMusicItemModel;
    }
    // endregion

    //
    // region inner Definition(s) ...
    private ServiceConnection mPlayerServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            //
            PlayerService.PlayerBinder mBinder = (PlayerService.PlayerBinder) iBinder;
            mPlayerService = mBinder.getService();
            isPlayerBound = true;

            //
            mMusicItemModel = mPlayerService.getItemModel();
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            //
            isPlayerBound = false;
        }
    };
    // endregion

}
