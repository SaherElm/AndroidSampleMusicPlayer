package ir.saherelm.testmusicplayer.requirements.viewholders;

import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ir.saherelm.testmusicplayer.requirements.interfaces.XItemModelClickListener;
import ir.saherelm.testmusicplayer.requirements.models.XBaseItemModel;

/**
 * a Base abstract RecyclerView's View Holder Class ...
 *
 * Created by SaherElm on 30/05/2018.
 */

@SuppressWarnings({"WeakerAccess", "unused"})
public abstract class XBaseViewHolder<TModel extends XBaseItemModel> extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

    private TModel itemModel;
    private XItemModelClickListener<TModel> itemClickCallback;

    //
    // region Constructor(s) ...
    public XBaseViewHolder(View itemView) {
        super(itemView);
    }

    public XBaseViewHolder(View itemView, XItemModelClickListener<TModel> itemClickCallback) {
        super(itemView);

        //
        this.itemClickCallback = itemClickCallback;
    }

    public XBaseViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent, @LayoutRes int layoutRes) {
        super(inflater.inflate(layoutRes, parent, false));
    }

    public XBaseViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent, @LayoutRes int layoutRes, XItemModelClickListener<TModel> itemClickCallback) {
        super(inflater.inflate(layoutRes, parent, false));

        //
        this.itemClickCallback = itemClickCallback;
    }
    // endregion

    public void bindTo(@NonNull TModel itemModel) {
        //
        this.itemModel = itemModel;

        //
        // adding ItemClickCallback to ItemView if Exists ...
        if (itemClickCallback != null) {
            //
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        //
        dataBinded(itemModel);
    }

    public abstract void dataBinded(TModel itemModel);

    public TModel getItemModel() {
        return itemModel;
    }

    @Override
    public void onClick(View view) {
        itemClickCallback.onItemClicked(itemModel);
    }

    @Override
    public boolean onLongClick(View view) {
        itemClickCallback.onItemLongClicked(itemModel);
        return true;
    }
}
