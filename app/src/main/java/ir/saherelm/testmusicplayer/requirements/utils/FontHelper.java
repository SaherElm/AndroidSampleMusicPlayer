package ir.saherelm.testmusicplayer.requirements.utils;

import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import ir.saherelm.testmusicplayer.requirements.XApplication;

/**
 * a helper class for providing Different data related to
 * Supported Fonts in App ...
 *
 * Created by SaherElm on 30/05/2018.
 */

@SuppressWarnings({"WeakerAccess", "unused"})
public class FontHelper {

    private static final String FONT_PREFIX = "fonts/";

    @SuppressWarnings("WeakerAccess")
    @StringDef({
            FONT_ARSHIA,
            FONT_KOODAK,
            FONT_NAZANIN,
            FONT_TRAFIC,
            FONT_ZAR,
            FONT_DAST_NEVIS,
            FONT_IRAN_SANS,
            FONT_IRAN_SANS_BOLD,
            FONT_IRAN_SANS_MOBILE,
            FONT_IRAN_SANS_MOBILE_BOLD
    })
    @Retention(RetentionPolicy.SOURCE)
    public @interface Fonts {}
    public static final String FONT_ARSHIA = FONT_PREFIX + "BArshia-Font.ttf";
    public static final String FONT_KOODAK = FONT_PREFIX + "BKoodk-Bold.ttf";
    public static final String FONT_NAZANIN = FONT_PREFIX + "BNazin-Bold.ttf";
    public static final String FONT_TRAFIC = FONT_PREFIX + "BTraffic-Bold.ttf";
    public static final String FONT_ZAR = FONT_PREFIX + "BZar-Bold.ttf";
    public static final String FONT_DAST_NEVIS = FONT_PREFIX + "DastNevis.ttf";
    public static final String FONT_IRAN_SANS = FONT_PREFIX + "IRAN-Sans.ttf";
    public static final String FONT_IRAN_SANS_BOLD = FONT_PREFIX + "IRAN-SansBold.ttf";
    public static final String FONT_IRAN_SANS_MOBILE = FONT_PREFIX + "IRAN-SansMobile.ttf";
    public static final String FONT_IRAN_SANS_MOBILE_BOLD = FONT_PREFIX + "IRAN-SansMobileBold.ttf";

    public Typeface getFontTypeface(@NonNull @Fonts String font) {
        return Typeface.createFromAsset(XApplication.getAppContext().getAssets(), font);
    }

}
