package ir.saherelm.testmusicplayer.requirements.adapters;

import android.support.annotation.Nullable;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import ir.saherelm.testmusicplayer.requirements.interfaces.XItemModelClickListener;
import ir.saherelm.testmusicplayer.requirements.models.MusicItemModel;
import ir.saherelm.testmusicplayer.requirements.viewholders.MusicItemViewHolder;

/**
 * the RecyclerView Adapter for Adapting MusicItems On Screen ...
 *
 * Created by SaherElm on 30/05/2018.
 */

@SuppressWarnings("unused")
public class MusicItemAdapter extends XBaseRecyclerViewAdapter<MusicItemModel, MusicItemViewHolder> {

    //
    // region Constructor(s) ...
    public MusicItemAdapter() {
        super();
    }

    public MusicItemAdapter(List<MusicItemModel> data) {
        super(data);
    }

    public MusicItemAdapter(XItemModelClickListener<MusicItemModel> itemClickCallback) {
        super(itemClickCallback);
    }

    public MusicItemAdapter(List<MusicItemModel> data, XItemModelClickListener<MusicItemModel> itemClickCallback) {
        super(data, itemClickCallback);
    }
    // endregion


    @Nullable
    @Override
    protected Comparator<MusicItemModel> getComparator() {
        return new Comparator<MusicItemModel>() {
            @Override
            public int compare(MusicItemModel item1, MusicItemModel item2) {
                return item1.getTitle().compareTo(item2.getTitle());
            }
        };
    }

    @Nullable
    @Override
    protected List<MusicItemModel> getFilterResult(String query) {
        List<MusicItemModel> mResult = new ArrayList<>();

        for (MusicItemModel item : getOriginalData()) {
            if (item.getTitle().toLowerCase().contains(query.toLowerCase())
                    || item.getDescription().toLowerCase().contains(query.toLowerCase())
                    || item.getSigner().toLowerCase().contains(query.toLowerCase())) {
                //
                mResult.add(item);
            }
        }

        return mResult.size() == 0 ? null : mResult;
    }

    @Override
    public MusicItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //
        return new MusicItemViewHolder(getInflater(), parent, getItemClickCallback()); //, itemWidth, itemHeight);
    }

}
