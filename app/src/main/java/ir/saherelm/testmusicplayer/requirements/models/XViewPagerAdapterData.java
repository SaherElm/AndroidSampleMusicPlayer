package ir.saherelm.testmusicplayer.requirements.models;

import android.support.annotation.DrawableRes;
import android.support.v4.app.Fragment;

/**
 * View Pager Adapter Data Model ...
 *
 * Created by SaherElm on 30/05/2018.
 */

@SuppressWarnings("unused")
public class XViewPagerAdapterData {

    private Fragment pageFragment;
    private String pageTitle;
    private @DrawableRes int pageIconResource;

    //
    // region Constructor(s) ...
    public XViewPagerAdapterData() {
    }

    public XViewPagerAdapterData(Fragment pageFragment, String pageTitle, int pageIconResource) {
        this.pageFragment = pageFragment;
        this.pageTitle = pageTitle;
        this.pageIconResource = pageIconResource;
    }
    // endregion

    //
    // region Getter(s) / Setter(s) ...
    public Fragment getPageFragment() {
        return pageFragment;
    }

    public void setPageFragment(Fragment pageFragment) {
        this.pageFragment = pageFragment;
    }

    public String getPageTitle() {
        return pageTitle;
    }

    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }

    public int getPageIconResource() {
        return pageIconResource;
    }

    public void setPageIconResource(int pageIconResource) {
        this.pageIconResource = pageIconResource;
    }
    // endregion

}
