package ir.saherelm.testmusicplayer.requirements;

import android.content.Context;
import android.support.multidex.MultiDexApplication;

import ir.saherelm.testmusicplayer.R;
import ir.saherelm.testmusicplayer.requirements.utils.FontHelper;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * the base Application Class ...
 *
 * Created by SaherElm on 30/05/2018.
 */

public class XApplication extends MultiDexApplication {

    private static Context appContext;

    @Override
    public void onCreate() {
        super.onCreate();
        appContext = this;

        //
        // "fonts/DastNevis.ttf"
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath(FontHelper.FONT_ARSHIA)
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }

    public static Context getAppContext() {
        return appContext;
    }

}
