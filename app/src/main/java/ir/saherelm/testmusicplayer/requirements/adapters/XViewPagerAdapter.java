package ir.saherelm.testmusicplayer.requirements.adapters;

import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import ir.saherelm.testmusicplayer.requirements.models.XViewPagerAdapterData;

/**
 * the ViewPager Adapter ...
 *
 * Created by SaherElm on 30/05/2018.
 */

@SuppressWarnings("unused")
public class XViewPagerAdapter extends FragmentStatePagerAdapter {

    private XViewPagerAdapterData[] pagesData;

    //
    // region Constructor(s) ...
    public XViewPagerAdapter(FragmentManager fm, @NonNull XViewPagerAdapterData[] pagesData) {
        super(fm);

        //
        this.pagesData = pagesData;
    }
    // endregion

    //
    // region Override Method(s) ...
    @Override
    public Fragment getItem(int position) {
        return pagesData[position].getPageFragment();
    }

    @Override
    public int getCount() {
        return pagesData == null ? 0 : pagesData.length;
    }
    // endregion

    //
    // region Protected Method(s) ...
    public CharSequence getPageTitle(int position) {
        return pagesData[position].getPageTitle();
    }

    public @DrawableRes int getPageIconRes(int position) {
        return pagesData[position].getPageIconResource();
    }

    public XViewPagerAdapterData getPageModel(int position) {
        return pagesData[position];
    }

    public XViewPagerAdapterData[] getPagesData() {
        return pagesData;
    }
    // endregion

}
