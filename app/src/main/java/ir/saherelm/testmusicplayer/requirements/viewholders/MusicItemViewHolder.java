package ir.saherelm.testmusicplayer.requirements.viewholders;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import ir.saherelm.testmusicplayer.R;
import ir.saherelm.testmusicplayer.requirements.interfaces.XItemModelClickListener;
import ir.saherelm.testmusicplayer.requirements.models.MusicItemModel;

/**
 * the View Holder for Music Items ...
 *
 * Created by SaherElm on 30/05/2018.
 */

@SuppressWarnings("unused")
public class MusicItemViewHolder extends XBaseViewHolder<MusicItemModel> {

    private TextView tvTitle;
    private TextView tvSigner;
    private TextView tvDescription;
    private ImageView ivImage;

    //
    // region Constructor(s) ...
    public MusicItemViewHolder(View itemView) {
        super(itemView);

        //
        prepareViews();
    }

    public MusicItemViewHolder(View itemView, XItemModelClickListener<MusicItemModel> itemClickCallback) {
        super(itemView, itemClickCallback);

        //
        prepareViews();
    }

    public MusicItemViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        super(inflater, parent, R.layout.music_item_layout);

        //
        prepareViews();
    }

    public MusicItemViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent, XItemModelClickListener<MusicItemModel> itemClickCallback) {
        super(inflater, parent, R.layout.music_item_layout, itemClickCallback);

        //
        prepareViews();
    }
    // endregion

    private void prepareViews() {
        //
        ivImage = itemView.findViewById(R.id.iv_image);
        tvTitle = itemView.findViewById(R.id.tv_title);
        tvSigner = itemView.findViewById(R.id.tv_signer);
        tvDescription = itemView.findViewById(R.id.tv_description);
    }

    @Override
    public void bindTo(@NonNull MusicItemModel itemModel) {
        super.bindTo(itemModel);

        //
        tvTitle.setText(itemModel.getTitle());
        tvSigner.setText(itemModel.getSigner());
        tvDescription.setText(itemModel.getDescription());

        //
        ivImage.setImageResource(itemModel.getImage());
    }

    @Override
    public void dataBinded(MusicItemModel itemModel) {
    }

    //
    // region Getter(s) / Setter(s) ...
    public TextView getTvTitle() {
        return tvTitle;
    }

    public TextView getTvSigner() {
        return tvSigner;
    }

    public TextView getTvDescription() {
        return tvDescription;
    }

    public ImageView getIvImage() {
        return ivImage;
    }
    // endregion

}
