package ir.saherelm.testmusicplayer.requirements.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.DrawableRes;

/**
 * this is Represent a Music Item ...
 *
 * Created by SaherElm on 30/05/2018.
 */

@SuppressWarnings("unused")
public class MusicItemModel extends XBaseItemModel implements Parcelable {

    private String title;
    private String signer;
    private String description;
    @DrawableRes
    private int image;
    private String file;

    //
    // region Constructors ...
    public MusicItemModel() {
    }

    public MusicItemModel(String title, String signer, String description, int image, String file) {
        this.title = title;
        this.signer = signer;
        this.description = description;
        this.image = image;
        this.file = file;
    }
    // endregion

    //
    // region Parcelabler ...
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.signer);
        dest.writeString(this.description);
        dest.writeInt(this.image);
        dest.writeString(this.file);
    }

    @SuppressWarnings("WeakerAccess")
    protected MusicItemModel(Parcel in) {
        this.title = in.readString();
        this.signer = in.readString();
        this.description = in.readString();
        this.image = in.readInt();
        this.file = in.readString();
    }

    public static final Parcelable.Creator<MusicItemModel> CREATOR = new Parcelable.Creator<MusicItemModel>() {
        @Override
        public MusicItemModel createFromParcel(Parcel source) {
            return new MusicItemModel(source);
        }

        @Override
        public MusicItemModel[] newArray(int size) {
            return new MusicItemModel[size];
        }
    };
    // endregion

    //
    // region Getter(s) / Setter(s) ...
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSigner() {
        return signer;
    }

    public void setSigner(String signer) {
        this.signer = signer;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }
    // endregion

    //
    // region Override ToString, Hash, and Equals Method(s) ...
    @Override
    public String toString() {
        return "MusicItemModel{" +
                "title='" + title + '\'' +
                ", signer='" + signer + '\'' +
                ", description='" + description + '\'' +
                ", image=" + image +
                ", file='" + file + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MusicItemModel that = (MusicItemModel) o;

        return image == that.image
                && (title != null ? title.equals(that.title) : that.title == null)
                && (signer != null ? signer.equals(that.signer) : that.signer == null)
                && (description != null ? description.equals(that.description) : that.description == null)
                && file.equals(that.file);
    }

    @Override
    public int hashCode() {
        int result = title != null ? title.hashCode() : 0;
        result = 31 * result + (signer != null ? signer.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + image;
        result = 31 * result + file.hashCode();
        return result;
    }
    // endregion

}
