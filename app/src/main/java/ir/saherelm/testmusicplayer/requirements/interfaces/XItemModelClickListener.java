package ir.saherelm.testmusicplayer.requirements.interfaces;

import ir.saherelm.testmusicplayer.requirements.models.XBaseItemModel;

/**
 * the Base Click Listener interface ...
 *
 * Created by SaherElm on 30/05/2018.
 */

public interface XItemModelClickListener<TModel extends XBaseItemModel> {
    void onItemClicked(TModel itemModel);
    void onItemLongClicked(TModel itemModel);
}
