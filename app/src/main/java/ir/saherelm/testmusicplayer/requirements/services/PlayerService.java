package ir.saherelm.testmusicplayer.requirements.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.StringDef;
import android.support.v7.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import ir.saherelm.testmusicplayer.R;
import ir.saherelm.testmusicplayer.requirements.models.MusicItemModel;
import ir.saherelm.testmusicplayer.ui.activities.PlayerActivity;

/**
 * this is Our Media Player Service which Play Selected Audio in Background
 * and Send Notifications to UI  ...
 *
 * Created by SaherElm on 30/05/2018.
 */

@SuppressWarnings("unused")
public class PlayerService
        extends Service
        implements MediaPlayer.OnCompletionListener,
                    MediaPlayer.OnPreparedListener,
                    MediaPlayer.OnErrorListener,
                    MediaPlayer.OnSeekCompleteListener {

    private final String TAG = "sh_" + getClass().getSimpleName();

    //
    private MediaPlayer mMediaPlayer;

    //
    // Set up the notification ID
    private static final int NOTIFICATION_ID = 1;
    private final Handler mHandler = new Handler();

    //
    public static final String ACTION_PLAY = "ir.saherelm.testmusicplayer.START_PLAYING";
    public static final String ACTION_NOTIFY_UI = "ir.saherelm.testmusicplayer.NOTIFY_UI";

    @StringDef({
            PARAM_CURRENT_TIME,
            PARAM_MAX_TIME,
            PARAM_IS_PLAYING
    })
    @Retention(RetentionPolicy.SOURCE)
    public @interface PlayerServiceInfo {}
    public static final String PARAM_CURRENT_TIME = "param:current_time";
    public static final String PARAM_MAX_TIME = "param:max_time";
    public static final String PARAM_IS_PLAYING = "param:is_playing";

    //
    private Intent mUpdateNotifierIntent;
    private MusicItemModel itemModel;
    private final IBinder mPlayerBinder = new PlayerBinder();


    //
    // region static Method(s) ...
    public static void startPlaying(Context context, MusicItemModel itemModel) {
        Intent intent = new Intent(context, PlayerService.class);
        intent.putExtra(PlayerActivity.PARAM_MUSIC_ITEM, itemModel);
        intent.setAction(ACTION_PLAY);

        context.startService(intent);
    }
    // endregion

    //
    // region Overrided Method(s) ...
    @Override
    public void onCreate() {
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        //
        destroyPlayer();
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return mPlayerBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        //
        destroyPlayer();

        //
        return super.onUnbind(intent);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //
        if (intent != null) {
            final String action = intent.getAction();
            if (!TextUtils.isEmpty(action)) {
                switch (action) {

                    case ACTION_PLAY:
                        //
                        MusicItemModel mItemModel = intent.getParcelableExtra(PlayerActivity.PARAM_MUSIC_ITEM);
                        if (mItemModel != null) {
                            setItemModel(mItemModel);
                        }
                        break;

                }
            }
        }
        return START_NOT_STICKY;
    }
    // endregion

    //
    // region Listener(s) Method(s) ...
    @Override
    public void onSeekComplete(MediaPlayer mp) {
        Log.i(TAG, "onSeekComplete");

        //
        if (!mMediaPlayer.isPlaying()){
            playMedia();
        }
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        Log.i(TAG, "onError");

        //
        switch (what) {
            case MediaPlayer.MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK:
                Toast.makeText(this,
                        "MEDIA ERROR NOT VALID FOR PROGRESSIVE PLAYBACK " + extra,
                        Toast.LENGTH_SHORT).show();
                break;
            case MediaPlayer.MEDIA_ERROR_SERVER_DIED:
                Toast.makeText(this, "MEDIA ERROR SERVER DIED " + extra,
                        Toast.LENGTH_SHORT).show();
                break;
            case MediaPlayer.MEDIA_ERROR_UNKNOWN:
                Toast.makeText(this, "MEDIA ERROR UNKNOWN " + extra,
                        Toast.LENGTH_SHORT).show();
                break;
        }
        return false;
    }

    @Override
    public void onPrepared(MediaPlayer arg0) {
        Log.i(TAG, "onPrepared");

        //
        playMedia();
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        Log.i(TAG, "onCompletion");

        //
        cancelNotification();
        LogMediaPosition();

        //
        stopMedia();
        stopSelf();
    }
    // endregion

    //
    // region Public Action Method(s) ...
    public void setItemModel(MusicItemModel itemModel) {
        this.itemModel = itemModel;

        //
        prepareMediaPlayer();
    }

    public MusicItemModel getItemModel() {
        return itemModel;
    }

    public void prepareMediaPlayer() {
        //
        if (mUpdateNotifierIntent == null) {
            mUpdateNotifierIntent = new Intent(ACTION_NOTIFY_UI);
        }

        //
        if (mMediaPlayer == null) {
            mMediaPlayer = new MediaPlayer();
            mMediaPlayer.setOnCompletionListener(this);
            mMediaPlayer.setOnErrorListener(this);
            mMediaPlayer.setOnPreparedListener(this);
            mMediaPlayer.setOnSeekCompleteListener(this);
            mMediaPlayer.reset();
        }

        //
        // Insert notification start
        initNotification();

        //
        mMediaPlayer.reset();

        //
        // Set up the MediaPlayer data source using the strAudioLink value
        if (!mMediaPlayer.isPlaying()) {
            try {
                //
                AssetFileDescriptor mAfd = getAssets().openFd("musics/" + itemModel.getFile());
                mMediaPlayer.setDataSource(mAfd.getFileDescriptor(), mAfd.getStartOffset(), mAfd.getLength());
                mAfd.close();

                //
                // Prepare mediaplayer
                mMediaPlayer.prepareAsync();
            }  catch (IOException e) {
                e.printStackTrace();
            }
        }

        //
        setupHandler();
    }

    public void playMedia() {
        //
        if (!mMediaPlayer.isPlaying()) {
            mMediaPlayer.start();
            initNotification();
        }

        //
        setupHandler();
    }

    public void pauseMedia() {
        //
        if (mMediaPlayer.isPlaying()) {
            mMediaPlayer.pause();
        }

        //
        mHandler.removeCallbacks(sendUpdatesToUI);
    }

    public void stopMedia() {
        //
        if (mMediaPlayer.isPlaying()) {
            mMediaPlayer.stop();
        }

        //
        mHandler.removeCallbacks(sendUpdatesToUI);
        cancelNotification();
    }

    public void updateSeekPos(int seekPos) {
        //
        if (mMediaPlayer.isPlaying()) {
            //
            mHandler.removeCallbacks(sendUpdatesToUI);
            mMediaPlayer.seekTo(seekPos);

            //
            setupHandler();
        } else {
            //
            mMediaPlayer.seekTo(seekPos);
        }
    }

    public boolean isPlayingMedia() {
        //
        if (mMediaPlayer != null
                && mMediaPlayer.isPlaying()) {
            //
            return true;
        }

        //
        return false;
    }
    // endregion

    //
    // region Private Method(s) ...
    private void destroyPlayer() {
        //
        if (mMediaPlayer != null) {
            if (mMediaPlayer.isPlaying()) {
                mMediaPlayer.stop();
            }
            mMediaPlayer.release();
        }

        //
        // Cancel the notification
        cancelNotification();

        //
        // Stop the seekbar mHandler from sending updates to UI
        mHandler.removeCallbacks(sendUpdatesToUI);
    }

    private void setupHandler() {
        mHandler.removeCallbacks(sendUpdatesToUI);
        mHandler.postDelayed(sendUpdatesToUI, 1000); // 1 second
    }

    private void LogMediaPosition() {
        //
        if (mMediaPlayer.isPlaying()) {
            //
            int currentTime = mMediaPlayer.getCurrentPosition();
            int maxTime = mMediaPlayer.getDuration();

            //
            mUpdateNotifierIntent.putExtra(PARAM_CURRENT_TIME, currentTime);
            mUpdateNotifierIntent.putExtra(PARAM_MAX_TIME, maxTime);
            mUpdateNotifierIntent.putExtra(PARAM_IS_PLAYING, true);
        } else {
            //
            mUpdateNotifierIntent.putExtra(PARAM_IS_PLAYING, false);
        }

        //
        sendBroadcast(mUpdateNotifierIntent);
    }

    private void initNotification() {
        //
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (mNotificationManager == null) {
            return;
        }

        //
        int icon = itemModel.getImage();
        CharSequence tickerText = getString(R.string.app_name);
        CharSequence contentTitle = itemModel.getTitle();
        CharSequence contentText = itemModel.getSigner();

        //
        Intent notificationIntent = new Intent(this, PlayerActivity.class);

        //
        PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(), 0,
                notificationIntent, 0);

        //
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext());

        //
        mBuilder.setAutoCancel(false);
        mBuilder.setTicker(tickerText);
        mBuilder.setContentTitle(contentTitle);
        mBuilder.setContentText(contentText);
        mBuilder.setSmallIcon(icon);
        mBuilder.setContentIntent(contentIntent);
        mBuilder.setOngoing(true);
        mBuilder.setNumber(100);

        //
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }

    private void cancelNotification() {
        //
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (mNotificationManager == null) {
            return;
        }

        //
        mNotificationManager.cancel(NOTIFICATION_ID);
    }
    // endregion

    //
    // region Inner Definition(s) ...
    private Runnable sendUpdatesToUI = new Runnable() {
        public void run() {
            //
            LogMediaPosition();

            //
            mHandler.postDelayed(this, 1000); // 2 seconds
        }
    };

    public class PlayerBinder extends Binder {
        public PlayerService getService() {
            return PlayerService.this;
        }
    }
    // endregion

}
