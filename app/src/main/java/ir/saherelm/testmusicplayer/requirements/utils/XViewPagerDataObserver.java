package ir.saherelm.testmusicplayer.requirements.utils;

import android.database.DataSetObserver;
import android.support.design.widget.TabLayout;

import ir.saherelm.testmusicplayer.R;
import ir.saherelm.testmusicplayer.requirements.adapters.XViewPagerAdapter;

/**
 * an Observer instance which attached to a ViewPager Adapter for
 * Handling Changed Event in ViewPager Adapter ...
 *
 * Created by SaherElm on 30/05/2018.
 */

public class XViewPagerDataObserver extends DataSetObserver {

    private TabLayout mTabLayout;
    private XViewPagerAdapter mAdapter;

    public XViewPagerDataObserver(TabLayout mTabLayout, XViewPagerAdapter mAdapter) {
        this.mTabLayout = mTabLayout;
        this.mAdapter = mAdapter;
    }

    @Override
    public void onChanged() {
        super.onChanged();

        //
        updateTabViews();
    }

    @Override
    public void onInvalidated() {
        super.onInvalidated();

        //
        updateTabViews();
    }

    public void updateTabViews() {
        for (int i = 0; i < mTabLayout.getTabCount(); i++) {
            TabLayout.Tab mTab = mTabLayout.getTabAt(i);
            if (mTab != null) {
                mTab.setCustomView(R.layout.tab_item_layout);
                mTab.setIcon(mAdapter.getPageIconRes(i));
            }
        }
    }
}
