package ir.saherelm.testmusicplayer.requirements.interfaces;

import ir.saherelm.testmusicplayer.requirements.models.XBaseItemModel;

/**
 * the base Callback interface ...
 *
 * Created by SaherElm on 30/05/2018.
 */

@SuppressWarnings("unused")
public interface XCallback<TModel extends XBaseItemModel> {
    void call(TModel itemModel);
}
