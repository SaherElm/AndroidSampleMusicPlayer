package ir.saherelm.testmusicplayer.requirements.providers;

import java.util.ArrayList;
import java.util.List;

import ir.saherelm.testmusicplayer.R;
import ir.saherelm.testmusicplayer.requirements.models.MusicItemModel;

/**
 * provide temparory data for using in app ...
 *
 * Created by SaherElm on 30/05/2018.
 */

public class TempDataProvider {

    public static List<MusicItemModel> getMusicItems() {
        //
        List<MusicItemModel> mResult = new ArrayList<>();

        //
        mResult.add(new MusicItemModel("لعنتی های دوست داشتنی", "رضا یزدانی", "", R.drawable.a1, "1.mp3"));
        mResult.add(new MusicItemModel("خداحافظ", "مهدی مقدم", "", R.drawable.a2, "2.mp3"));
        mResult.add(new MusicItemModel("نازک دل حساس", "پویا بیاتی", "", R.drawable.a3, "3.mp3"));
        mResult.add(new MusicItemModel("راز سر بسته", "رضا صادقی", "", R.drawable.a4, "4.mp3"));
        mResult.add(new MusicItemModel("یازده ستاره", "سالار عقیلی", "", R.drawable.a5, "5.mp3"));
        mResult.add(new MusicItemModel("رهایم نکن", "محمد اصفهانی", "", R.drawable.a6, "6.mp3"));
        //
        mResult.add(new MusicItemModel("لعنتی های دوست داشتنی", "رضا یزدانی", "", R.drawable.a1, "1.mp3"));
        mResult.add(new MusicItemModel("خداحافظ", "مهدی مقدم", "", R.drawable.a2, "2.mp3"));
        mResult.add(new MusicItemModel("نازک دل حساس", "پویا بیاتی", "", R.drawable.a3, "3.mp3"));
        mResult.add(new MusicItemModel("راز سر بسته", "رضا صادقی", "", R.drawable.a4, "4.mp3"));
        mResult.add(new MusicItemModel("یازده ستاره", "سالار عقیلی", "", R.drawable.a5, "5.mp3"));
        mResult.add(new MusicItemModel("رهایم نکن", "محمد اصفهانی", "", R.drawable.a6, "6.mp3"));
        //
        mResult.add(new MusicItemModel("لعنتی های دوست داشتنی", "رضا یزدانی", "", R.drawable.a1, "1.mp3"));
        mResult.add(new MusicItemModel("خداحافظ", "مهدی مقدم", "", R.drawable.a2, "2.mp3"));
        mResult.add(new MusicItemModel("نازک دل حساس", "پویا بیاتی", "", R.drawable.a3, "3.mp3"));
        mResult.add(new MusicItemModel("راز سر بسته", "رضا صادقی", "", R.drawable.a4, "4.mp3"));
        mResult.add(new MusicItemModel("یازده ستاره", "سالار عقیلی", "", R.drawable.a5, "5.mp3"));
        mResult.add(new MusicItemModel("رهایم نکن", "محمد اصفهانی", "", R.drawable.a6, "6.mp3"));
        //
        mResult.add(new MusicItemModel("لعنتی های دوست داشتنی", "رضا یزدانی", "", R.drawable.a1, "1.mp3"));
        mResult.add(new MusicItemModel("خداحافظ", "مهدی مقدم", "", R.drawable.a2, "2.mp3"));
        mResult.add(new MusicItemModel("نازک دل حساس", "پویا بیاتی", "", R.drawable.a3, "3.mp3"));
        mResult.add(new MusicItemModel("راز سر بسته", "رضا صادقی", "", R.drawable.a4, "4.mp3"));
        mResult.add(new MusicItemModel("یازده ستاره", "سالار عقیلی", "", R.drawable.a5, "5.mp3"));
        mResult.add(new MusicItemModel("رهایم نکن", "محمد اصفهانی", "", R.drawable.a6, "6.mp3"));
        //
        mResult.add(new MusicItemModel("لعنتی های دوست داشتنی", "رضا یزدانی", "", R.drawable.a1, "1.mp3"));
        mResult.add(new MusicItemModel("خداحافظ", "مهدی مقدم", "", R.drawable.a2, "2.mp3"));
        mResult.add(new MusicItemModel("نازک دل حساس", "پویا بیاتی", "", R.drawable.a3, "3.mp3"));
        mResult.add(new MusicItemModel("راز سر بسته", "رضا صادقی", "", R.drawable.a4, "4.mp3"));
        mResult.add(new MusicItemModel("یازده ستاره", "سالار عقیلی", "", R.drawable.a5, "5.mp3"));
        mResult.add(new MusicItemModel("رهایم نکن", "محمد اصفهانی", "", R.drawable.a6, "6.mp3"));
        //
        mResult.add(new MusicItemModel("لعنتی های دوست داشتنی", "رضا یزدانی", "", R.drawable.a1, "1.mp3"));
        mResult.add(new MusicItemModel("خداحافظ", "مهدی مقدم", "", R.drawable.a2, "2.mp3"));
        mResult.add(new MusicItemModel("نازک دل حساس", "پویا بیاتی", "", R.drawable.a3, "3.mp3"));
        mResult.add(new MusicItemModel("راز سر بسته", "رضا صادقی", "", R.drawable.a4, "4.mp3"));
        mResult.add(new MusicItemModel("یازده ستاره", "سالار عقیلی", "", R.drawable.a5, "5.mp3"));
        mResult.add(new MusicItemModel("رهایم نکن", "محمد اصفهانی", "", R.drawable.a6, "6.mp3"));
        //
        mResult.add(new MusicItemModel("لعنتی های دوست داشتنی", "رضا یزدانی", "", R.drawable.a1, "1.mp3"));
        mResult.add(new MusicItemModel("خداحافظ", "مهدی مقدم", "", R.drawable.a2, "2.mp3"));
        mResult.add(new MusicItemModel("نازک دل حساس", "پویا بیاتی", "", R.drawable.a3, "3.mp3"));
        mResult.add(new MusicItemModel("راز سر بسته", "رضا صادقی", "", R.drawable.a4, "4.mp3"));
        mResult.add(new MusicItemModel("یازده ستاره", "سالار عقیلی", "", R.drawable.a5, "5.mp3"));
        mResult.add(new MusicItemModel("رهایم نکن", "محمد اصفهانی", "", R.drawable.a6, "6.mp3"));
        //
        mResult.add(new MusicItemModel("لعنتی های دوست داشتنی", "رضا یزدانی", "", R.drawable.a1, "1.mp3"));
        mResult.add(new MusicItemModel("خداحافظ", "مهدی مقدم", "", R.drawable.a2, "2.mp3"));
        mResult.add(new MusicItemModel("نازک دل حساس", "پویا بیاتی", "", R.drawable.a3, "3.mp3"));
        mResult.add(new MusicItemModel("راز سر بسته", "رضا صادقی", "", R.drawable.a4, "4.mp3"));
        mResult.add(new MusicItemModel("یازده ستاره", "سالار عقیلی", "", R.drawable.a5, "5.mp3"));
        mResult.add(new MusicItemModel("رهایم نکن", "محمد اصفهانی", "", R.drawable.a6, "6.mp3"));
        //
        mResult.add(new MusicItemModel("لعنتی های دوست داشتنی", "رضا یزدانی", "", R.drawable.a1, "1.mp3"));
        mResult.add(new MusicItemModel("خداحافظ", "مهدی مقدم", "", R.drawable.a2, "2.mp3"));
        mResult.add(new MusicItemModel("نازک دل حساس", "پویا بیاتی", "", R.drawable.a3, "3.mp3"));
        mResult.add(new MusicItemModel("راز سر بسته", "رضا صادقی", "", R.drawable.a4, "4.mp3"));
        mResult.add(new MusicItemModel("یازده ستاره", "سالار عقیلی", "", R.drawable.a5, "5.mp3"));
        mResult.add(new MusicItemModel("رهایم نکن", "محمد اصفهانی", "", R.drawable.a6, "6.mp3"));
        //
        mResult.add(new MusicItemModel("لعنتی های دوست داشتنی", "رضا یزدانی", "", R.drawable.a1, "1.mp3"));
        mResult.add(new MusicItemModel("خداحافظ", "مهدی مقدم", "", R.drawable.a2, "2.mp3"));
        mResult.add(new MusicItemModel("نازک دل حساس", "پویا بیاتی", "", R.drawable.a3, "3.mp3"));
        mResult.add(new MusicItemModel("راز سر بسته", "رضا صادقی", "", R.drawable.a4, "4.mp3"));
        mResult.add(new MusicItemModel("یازده ستاره", "سالار عقیلی", "", R.drawable.a5, "5.mp3"));
        mResult.add(new MusicItemModel("رهایم نکن", "محمد اصفهانی", "", R.drawable.a6, "6.mp3"));

        //
        return mResult;
    }


}
