package ir.saherelm.testmusicplayer.requirements;

import android.content.Context;
import android.support.annotation.IdRes;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * base Activity of Application ...
 *
 * Created by SaherElm on 30/05/2018.
 */

@SuppressWarnings("UnusedReturnValue")
public abstract class XBaseActivity extends AppCompatActivity {

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    protected ActionBar initToolbar(@IdRes int toolbarId) {
        return initToolbar((Toolbar) findViewById(toolbarId));
    }

    protected ActionBar initToolbar(Toolbar toolbar) {
        //
        if (toolbar == null) {
            throw new IllegalArgumentException("Toolbar not Found ...");
        }

        //
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar == null) {
            throw new IllegalArgumentException("Toolbar Couldn't be Settled ...");
        }

        //
        return actionBar;
    }


}
