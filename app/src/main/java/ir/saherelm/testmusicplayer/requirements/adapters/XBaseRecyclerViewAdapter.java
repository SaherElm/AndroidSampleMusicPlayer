package ir.saherelm.testmusicplayer.requirements.adapters;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import ir.saherelm.testmusicplayer.requirements.interfaces.XItemModelClickListener;
import ir.saherelm.testmusicplayer.requirements.models.XBaseItemModel;
import ir.saherelm.testmusicplayer.requirements.viewholders.XBaseViewHolder;

/**
 * the base Recycler View Adapter Class ...
 *
 * Created by SaherElm on 30/05/2018.
 */

@SuppressWarnings({"WeakerAccess", "unused"})
public abstract class XBaseRecyclerViewAdapter<TModel extends XBaseItemModel, THolder extends XBaseViewHolder<TModel>>
        extends RecyclerView.Adapter<THolder>
        implements Filterable {

    private final Object mLock = new Object();
    private XItemModelClickListener<TModel> itemClickCallback;
    private List<TModel> data;
    private List<TModel> originalData;
    private LayoutInflater mInflater;

    //
    // region Constructor(s) ...
    public XBaseRecyclerViewAdapter() {
        this(null, null);
    }

    public XBaseRecyclerViewAdapter(List<TModel> data) {
        this(data, null);
    }

    public XBaseRecyclerViewAdapter(XItemModelClickListener<TModel> itemClickCallback) {
        this(null, itemClickCallback);
    }

    public XBaseRecyclerViewAdapter(List<TModel> data, XItemModelClickListener<TModel> itemClickCallback) {
        //
        this.itemClickCallback = itemClickCallback;
        if (data != null) {
            this.data = data;
            this.originalData = data;
        } else {
            this.data = new ArrayList<>();
            this.originalData = new ArrayList<>();
        }
    }
    // endregion

    //
    // region Overrided Method(s) ...
    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);

        //
        mInflater = LayoutInflater.from(recyclerView.getContext());
    }

    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);

        //
        mInflater = null;
    }

    @Override
    public void onBindViewHolder(THolder holder, int position) {
        holder.bindTo(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                List<TModel> filterResult;
                if (TextUtils.isEmpty(charSequence)) {
                    filterResult = originalData;
                } else {
                    filterResult = getFilterResult(charSequence.toString().toLowerCase().trim());
                }

                FilterResults mFilterResults = new FilterResults();
                mFilterResults.values = filterResult;

                return mFilterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                if (filterResults == null) {
                    return;
                }

                //noinspection unchecked
                data = (List<TModel>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
    // endregion

    //
    // region Protected Method(s) ...
    protected List<TModel> getData() {
        return data;
    }

    protected List<TModel> getOriginalData() {
        return originalData;
    }

    protected TModel getDataItemModel(int position) {
        try {
            return data.get(position);
        } catch (Exception ex) {
            return null;
        }
    }

    protected TModel getOriginalDataItemModel(int position) {
        try {
            return originalData.get(position);
        } catch (Exception ex) {
            return null;
        }
    }

    protected int getOriginalItemCout() {
        return originalData == null ? 0 : originalData.size();
    }

    protected LayoutInflater getInflater() {
        return mInflater;
    }

    protected XItemModelClickListener<TModel> getItemClickCallback() {
        return itemClickCallback;
    }
    // endregion

    //
    // region Data Manipulation Method(s) ...
    public synchronized void clear() {
        synchronized (mLock) {
            data.clear();
            updateOriginalData();
        }

        notifyDataSetChanged();
    }

    public synchronized void add(TModel itemModel) {
        int index = data.size();

        synchronized (mLock) {
            data.add(itemModel);
            updateOriginalData();
        }

        notifyItemInserted(index);
    }

    public synchronized void addList(List<TModel> itemModelList) {
        int index = data.size();

        synchronized (mLock) {
            data.addAll(index, itemModelList);
            updateOriginalData();
        }

        notifyItemRangeInserted(index, itemModelList.size());
    }

    public synchronized void insert(int index, TModel itemModel) {
        if (data.size() < index) {
            return;
        }

        synchronized (mLock) {
            data.add(index + 1, itemModel);
            updateOriginalData();
        }

        notifyItemInserted(index + 1);
    }

    public synchronized void insert(int index, List<TModel> items) {
        if (data.size() < index) {
            return;
        }

        synchronized (mLock) {
            for (int i = 0; i < items.size(); i++) {
                data.add(index + i, items.get(i));
            }

            updateOriginalData();
        }

        notifyItemRangeInserted(index, items.size());
    }

    public synchronized void move(int fromIndex, int toIndex) {
        if (data.size() < fromIndex || data.size() < toIndex) {
            return;
        }

        synchronized (mLock) {
            data.set(toIndex, data.set(fromIndex, data.get(toIndex)));
            updateOriginalData();
        }

        notifyItemMoved(fromIndex, toIndex);
    }

    public synchronized void remove(int index) {
        if (data.size() < index) {
            return;
        }

        synchronized (mLock) {
            data.remove(index);
            updateOriginalData();
        }

        notifyItemRemoved(index);
    }

    public synchronized void remove(int index, int count) {
        if (data.size() < index + count) {
            return;
        }

        synchronized (mLock) {
            for (int i = 0; i < count; i++) {
                data.remove(i);
            }

            updateOriginalData();
        }

        notifyItemRangeRemoved(index, count);
    }

    public void sort() {
        synchronized (mLock) {
            Collections.sort(data, getComparator());

            updateOriginalData();
        }

        notifyDataSetChanged();
    }
    // endregion

    //
    // region private Method(s) ...
    private synchronized void updateOriginalData() {
        originalData = data;
    }
    // endregion

    //
    // region Abstract Method(s) ...
    @Nullable
    protected abstract Comparator<TModel> getComparator();

    @Nullable
    protected abstract List<TModel> getFilterResult(String query);
    // endregion

}
